from library_api.controllers.reader_controller import get_reader
from library_api.controllers.reader_controller import post_reader
from library_api.controllers.ticket_controller import get_ticket
from datetime import date
from library_api.controllers.order_book_controller import add_order_book
from library_api.controllers.tasks_controller import add_task
from library_api.controllers.user_request import user_get_bpmn2
from library_api.controllers.sql_request import sql_insert_task_bpmn2


def bmpn2():
    dict_from_reader = user_get_bpmn2()
    return_ticket = get_ticket(dict_from_reader['ticket_id'])
    if return_ticket is None:
        return 'ValueError', "Ticket not exist"
    z = 0
    w = len(dict_from_reader['literature_id'])
    debt = []
    for k in return_ticket.tasks:
        for i in k.order_book_list:
            if k.expect_return_date < date.today() and i.status == 0:
                debt.append(i.id_literature)
                z = z + 1
                if z >= 3:
                    return 404, k.order_book_list
    for k in return_ticket.tasks:
        for i in k.order_book_list:
            w = w + 1
            if w >= 16:
                return 405, w-1
    status, order_book = add_order_book(literature_id=dict_from_reader['literature_id'])
    if status == ValueError:
        return status, order_book
    status, task = add_task(order_book_es=order_book, ticket=return_ticket)
    if status == ValueError:
        return status, task
    return_ticket.tasks = []
    return_ticket.tasks.append(task)
    sql_insert_task_bpmn2(return_ticket)
    return 201, return_ticket