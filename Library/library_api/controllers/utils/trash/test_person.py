from mock import patch
from library_api.controllers.utils.trash.person import Person


@patch('library_api.controllers.utils.trash.person.get_name')
def test_name(mock_get_name):
    mock_get_name.return_value = ("Bob", "B")
    person = Person()
    name, n = person.name()
    assert name == "Bob"
    assert n == "B"

test_name()