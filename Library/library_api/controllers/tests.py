import datetime

from library_api.controllers.bpmn1 import bpmn1
from mock import patch, MagicMock
import unittest
from library_api.controllers.bpmn2 import bmpn2
from library_api.controllers.bpmn3 import bmpn3
from library_api.controllers.random_data import random_reader, random_registration, random_ticket, random_order_book, \
    random_material_literature, random_material_literature_3
from library_api.models import Tasks, Ticket


class TestReader(unittest.TestCase):
    """
    Our basic test class
    """

    def setUp(self):
        pass

    @patch("library_api.controllers.bpmn1.user_registration")
    @patch("library_api.controllers.bpmn1.user_find")
    @patch("library_api.controllers.reader_controller.sql_select_reader")
    def test_valid(self, mock_sql_select_reader, mock_user_find, mock_user_registration):
        find_data = ("Veronika", "89253626542")
        data_registration = random_registration()
        print('\n', '-------------------')
        print('TEST VALID BPMN1', '\n', '-------------------')
        print("READER:", find_data, '\n')
        print("READER", data_registration, '\n')
        mock_user_find.return_value = find_data
        mock_sql_select_reader.return_value = None
        mock_user_registration.return_value = data_registration
        status, reader = bpmn1()
        print('STATUS:', status)
        print('NEW READER:', reader, '\n')
        self.assertEqual(status, 201)

    @patch("library_api.controllers.bpmn1.user_find")
    @patch("library_api.controllers.reader_controller.sql_select_reader")
    def test_exists_reader(self, mock_sql_select_reader, mock_user_find):
        find_data = ("Egor", "89253626542")
        print('\n', '-------------------')
        print('TEST EXISTS READER BPMN1', '\n', '-------------------')
        print("READER:", find_data, '\n')
        print('SQL SELECT RETURN NOT NULL DATA')
        mock_user_find.return_value = find_data
        mock_sql_select_reader.return_value = random_reader()
        error, message = bpmn1()
        self.assertEqual(error, 'ValueError')
        self.assertEqual(message, 'Reader already exist')
        print('ERROR:', error)
        print('MESSAGE:', message, '\n')


class TestLiterature(unittest.TestCase):
    def setUp(self):
        ...

    @patch("library_api.controllers.order_book_controller.sql_select_literature")
    @patch("library_api.controllers.ticket_controller.sql_select_ticket")
    @patch("library_api.controllers.bpmn2.user_get_bpmn2")
    def test_valid(self, mock_user_get_bpmn2, mock_sql_select_ticket, mock_sql_select_literature):
        print('\n', '-------------------')
        print('TEST VALID BPMN2', '\n', '-------------------')
        data_from_reader = {'ticket_id': 1, 'literature_id': [5]}
        data_ticket = random_ticket()
        data_ticket.unique_key = 1
        mock_user_get_bpmn2.return_value = data_from_reader
        mock_sql_select_ticket.return_value = data_ticket
        mock_sql_select_literature.return_value = random_material_literature()
        print("READER:", data_from_reader)
        print('TICKET:', data_ticket)
        status, task = bmpn2()
        print(status)
        self.assertEqual(status, 201)

    @patch("library_api.controllers.ticket_controller.sql_select_ticket")
    @patch("library_api.controllers.bpmn2.user_get_bpmn2")
    def test_debt_library(self, mock_user_get_bpmn2, mock_sql_select_ticket):
        print('\n', '-------------------')
        print('TEST DEBT BPMN2', '\n', '-------------------')
        data_from_reader = {'ticket_id': 1, 'literature_id': [5]}
        data_ticket = random_ticket()
        data_ticket.unique_key = 1
        data_ticket.tasks[0].expect_return_date = datetime.date.today() - datetime.timedelta(days=10)
        data_ticket.tasks[0].addOrderBooksList(random_order_book())
        mock_user_get_bpmn2.return_value = data_from_reader
        mock_sql_select_ticket.return_value = data_ticket
        print("READER:", data_from_reader)
        print('TICKET:', data_ticket)
        status, task = bmpn2()
        print('STATUS:', status)
        print("MESSAGE: Debt more or equally 3:", task)
        self.assertEqual(status, 404)

    @patch("library_api.controllers.order_book_controller.sql_select_literature")
    @patch("library_api.controllers.ticket_controller.sql_select_ticket")
    @patch("library_api.controllers.bpmn2.user_get_bpmn2")
    def test_literature_nonexisten_number(self, mock_user_get_bpmn2, mock_sql_select_ticket, mock_sql_select_literature):
        print('\n', '-------------------')
        print('TEST VALID BPMN2', '\n', '-------------------')
        data_from_reader = {'ticket_id': 1, 'literature_id': [5]}
        data_ticket = random_ticket()
        data_ticket.unique_key = 1
        mock_user_get_bpmn2.return_value = data_from_reader
        mock_sql_select_ticket.return_value = data_ticket
        mock_sql_select_literature.return_value = 0
        print("READER:", data_from_reader)
        print('TICKET:', data_ticket)
        status, task = bmpn2()
        print("STATUS:", status)
        print("MESSAGE:", task)
        self.assertEqual(task, "Literature not exist (number)")

    @patch("library_api.controllers.ticket_controller.sql_select_ticket")
    @patch("library_api.controllers.bpmn2.user_get_bpmn2")
    def test_in_all_task_more_then_15(self, mock_user_get_bpmn2, mock_sql_select_ticket):
        print('\n', '-------------------')
        print('TEST MORE THEN 15 BPMN2', '\n', '-------------------')
        data_from_reader = {'ticket_id': 1, 'literature_id': [5]}
        data_ticket = Ticket()
        data_ticket.tasks = []
        data_ticket.unique_key = 1
        for i in range(5):
            if i != 5:
                data_ticket.tasks.append(Tasks())
            data_ticket.tasks[i].registration_date = datetime.date.today()
            ord_book = [random_order_book() for i in range(3)]
            data_ticket.tasks[i].addOrderBooksList(ord_book)
        mock_user_get_bpmn2.return_value = data_from_reader
        mock_sql_select_ticket.return_value = data_ticket
        print("READER:", data_from_reader)
        print('TICKET:', data_ticket)
        status, task = bmpn2()
        print('STATUS:', status)
        print("MESSAGE:", "Books on hand:", task)
        self.assertEqual(status, 405)

    @patch("library_api.controllers.order_book_controller.sql_select_literature")
    @patch("library_api.controllers.ticket_controller.sql_select_ticket")
    @patch("library_api.controllers.bpmn2.user_get_bpmn2")
    def test_in_one_task_more_then_5(self, mock_user_get_bpmn2, mock_sql_select_ticket, mock_sql_select_literature):
        print('\n', '-------------------')
        print('TEST MORE THEN 5 IN ONE TASK BPMN2', '\n', '-------------------')
        data_from_reader = {'ticket_id': 1, 'literature_id': [5, 6, 7, 8, 9]}
        data_ticket = random_ticket()
        data_ticket.unique_key = 1
        mock_user_get_bpmn2.return_value = data_from_reader
        mock_sql_select_ticket.return_value = data_ticket
        mock_sql_select_literature.return_value = [random_material_literature() for _ in range(5)]
        print("READER:", data_from_reader)
        print('TICKET:', data_ticket)
        status, task = bmpn2()
        print('STATUS:', status)
        print("MESSAGE: ", task)
        self.assertEqual(str(task), 'Len tasks more, then 5')


class TestReturnLiterature(unittest.TestCase):
    """
    Our basic test class
    """

    def setUp(self):
        ...

    @patch("library_api.controllers.bpmn3.sql_get_bpmn3_all")
    @patch("library_api.controllers.bpmn3.sql_find_order_literarture")
    @patch("library_api.controllers.ticket_controller.sql_select_ticket")
    @patch("library_api.controllers.bpmn3.user_get_bpmn2")
    def test_valid(self, mock_user_get_bpmn2, mock_sql_select_ticket, mock_sql_find_order_literarture,
                   mock_sql_get_bpmn3_all):
        print('\n', '-------------------')
        print('TEST VALID BPMN3', '\n', '-------------------')
        ticket_data = {'ticket_id': 1, 'literature_id': 5}
        mock_user_get_bpmn2.return_value = ticket_data
        mock_sql_select_ticket.return_value = random_ticket()
        order_books = random_order_book()
        order_book = order_books[0]
        order_book.status = 0
        order_book.fact_return_date = None
        mock_sql_find_order_literarture.return_value = order_book
        mock_sql_get_bpmn3_all.return_value = None
        print("READER:", ticket_data)
        print("ORDER LITERATURE BEFORE:", order_book)
        status, message = bmpn3()
        print("STATUS", status)
        print("ORDER LITERARTURE AFTER", message)
        print("BOOK RETURN")
        self.assertEqual(status, 200)

    @patch("library_api.controllers.bpmn3.sql_get_bpmn3_all")
    @patch("library_api.controllers.bpmn3.sql_find_order_literarture")
    @patch("library_api.controllers.ticket_controller.sql_select_ticket")
    @patch("library_api.controllers.bpmn3.user_get_bpmn2")
    def test_book_in_other_reader(self, mock_user_get_bpmn2, mock_sql_select_ticket, mock_sql_find_order_literarture,
                   mock_sql_get_bpmn3_all):
        print('\n', '-------------------')
        print('TEST VALID BPMN3', '\n', '-------------------')
        ticket_data = {'ticket_id': 1, 'literature_id': 5}
        mock_user_get_bpmn2.return_value = ticket_data
        mock_sql_select_ticket.return_value = random_ticket()
        order_books = random_order_book()
        order_book = order_books[0]
        order_book.status = 0
        mock_sql_get_bpmn3_all.return_value = (3, order_book)
        mock_sql_find_order_literarture.return_value = None
        print("READER:", ticket_data)
        status, message = bmpn3()
        print("STATUS", status)
        print("MESSAGE", message)
        print("BOOK RETURN, BUT FOR OTHER READER")

    @patch("library_api.controllers.bpmn3.sql_get_bpmn3_all")
    @patch("library_api.controllers.bpmn3.sql_find_order_literarture")
    @patch("library_api.controllers.ticket_controller.sql_select_ticket")
    @patch("library_api.controllers.bpmn3.user_get_bpmn2")
    def test_book_is_not_exist(self, mock_user_get_bpmn2, mock_sql_select_ticket, mock_sql_find_order_literarture,
                   mock_sql_get_bpmn3_all):
        print('\n', '-------------------')
        print('TEST VALID BPMN3', '\n', '-------------------')
        ticket_data = {'ticket_id': 1, 'literature_id': 5}
        mock_user_get_bpmn2.return_value = ticket_data
        mock_sql_select_ticket.return_value = random_ticket()
        order_books = random_order_book()
        order_book = order_books[0]
        order_book.status = 0
        mock_sql_get_bpmn3_all.return_value = None, "not found"
        mock_sql_find_order_literarture.return_value = None
        print("READER:", ticket_data)
        status, message = bmpn3()
        print("STATUS", status)
        print("MESSAGE", message)
        self.assertEqual(message, "The book not from this library")

    @patch("library_api.controllers.ticket_controller.sql_select_ticket")
    @patch("library_api.controllers.bpmn3.user_get_bpmn2")
    def test_ticket_is_not_exist(self, mock_user_get_bpmn2, mock_sql_select_ticket):
        print('\n', '-------------------')
        print('TEST NOT EXIST BPMN3', '\n', '-------------------')
        ticket_data = {'ticket_id': 1, 'literature_id': 5}
        mock_user_get_bpmn2.return_value = ticket_data
        mock_sql_select_ticket.return_value = None
        print("READER:", ticket_data)
        status, message = bmpn3()
        print("STATUS", status)
        print("MESSAGE", message)
        self.assertEqual(message, "Ticket not exist")


if __name__ == '__main__':
    import unittest
    unittest.main()


