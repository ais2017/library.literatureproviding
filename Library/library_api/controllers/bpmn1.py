from library_api.controllers.user_request import user_find
from library_api.controllers.sql_request import sql_select_reader
from library_api.controllers.user_request import user_find, user_registration
from library_api.controllers.reader_controller import get_reader
from library_api.controllers.reader_controller import post_reader
from library_api.models import Reader


def bpmn1():
    lastName, phone_number = user_find()
    return_reader = get_reader(lastName=lastName, phone_number=phone_number)
    if return_reader is not None:
        return 'ValueError', "Reader already exist"
    dict_registration = user_registration()
    dict_registration['last_name'] = lastName
    dict_registration['phone_number'] = phone_number
    ticket = post_reader(data=Reader().from_dict(dict_registration))
    return 201, ticket