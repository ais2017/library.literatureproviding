from library_api.controllers.reader_controller import get_reader
from library_api.controllers.reader_controller import post_reader
from library_api.controllers.ticket_controller import get_ticket
from datetime import date
from library_api.controllers.order_book_controller import add_order_book
from library_api.controllers.tasks_controller import add_task
from library_api.controllers.user_request import user_get_bpmn2
from library_api.controllers.sql_request import sql_find_order_literarture, sql_get_bpmn3_all, \
    sql_insert_order_literature


def bmpn3():
    ticket_id, literature_id = user_get_bpmn2()
    return_ticket = get_ticket(ticket_id)
    if return_ticket is None:
        return ValueError, "Ticket not exist"
    order_book = sql_find_order_literarture(return_ticket, literature_id)
    if order_book is not None:
            order_book.status = 1
            sql_insert_order_literature(ticket_id, order_book)
            return 200, order_book
    else:
        unique_id, order_book = sql_get_bpmn3_all(literature_id)
        if unique_id is not None:
            order_book.status = 0
            sql_insert_order_literature(unique_id, order_book)
            return ValueError, "The book in other reader"
        else:
            return ValueError, "The book not from this library"
