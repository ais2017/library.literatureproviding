from library_api.models.reader import Reader
from library_api.models.order_book import OrderBook
from library_api.models.literature import Literature
from library_api.models.tasks import Tasks
from library_api.models.ticket import Ticket
from faker import Faker
import datetime
faker = Faker()

phone_number = '8(932)441-22-98', '8(971)565-56-89', '8(964)408-40-71'


def random_registration():
    return {'email': faker.email(), 'first_name': faker.first_name(), 'pasport_id': faker.postcode(),
            'pasport_series': faker.postcode(), 'patronymic': faker.first_name(),
            'phone_number': faker.random_element(phone_number)}


def random_reader():
    return Reader(email= faker.email(), first_name = faker.first_name(), id = faker.random_int(0, 600),
                             last_name= faker.last_name(), pasport_id= faker.postcode(), pasport_series = faker.postcode(),
                             patronymic = faker.first_name(), phone_number=faker.random_element(phone_number))


def random_material_literature():
    literatures = []
    literature = Literature(id=faker.random_int(0, 600), title=faker.name(), author=faker.first_name(),
                        publishing_house=faker.company(), year_publishing=faker.year())
    literature.number_literature = faker.random_int(0, 600)
    literatures.append(literature)
    return literatures


def random_material_literature_3():
    literatures = []
    for _ in range(3):
        literature = Literature(id=faker.random_int(0, 600), title=faker.name(), author=faker.first_name(),
                            publishing_house=faker.company(), year_publishing=faker.year())
        literature.electronic_address=faker.email()
        literatures.append(literature)
    return literatures


def random_electronic_literature():
    literatures = []
    for _ in range(5):
        literature = Literature(id=faker.random_int(0, 600), title=faker.name(), author=faker.first_name(),
                            publishing_house=faker.company(), year_publishing=faker.year())
        literature.electronic_address=faker.email()
        literatures.append(literature)
    return literatures


def random_electronic_literature_():
    literatures = []
    for _ in range(10):
        literature = Literature(id=faker.random_int(0, 600), title=faker.name(), author=faker.first_name(),
                            publishing_house=faker.company(), year_publishing=faker.year())
        literature.electronic_address=faker.email()
        literatures.append(literature)
    return literatures


def random_material_literature_():
    literatures = []
    for _ in range(4):
        literature = Literature(id=faker.random_int(0, 600), title=faker.name(), author=faker.first_name(),
                            publishing_house=faker.company(), year_publishing=faker.year())
        literature.number_literature = faker.random_int(0, 600)
        literatures.append(literature)
    return literatures


def random_order_book(literature = None, status=0):
    if literature is None:
        literature = random_material_literature_3()
    order_books = []
    for z, i in enumerate(literature):
        order_book = OrderBook(id_literature=i)
        order_book.status = status
        order_books.append(order_book)
    return order_books


def random_order_book_(literature = None, status=0):
    if literature is None:
        literature = random_material_literature_()
    order_books = []
    for z, i in enumerate(literature):
        order_book = OrderBook(id_literature=i)
        order_book.status = status
        order_books.append(order_book)
    return order_books


def random_tasks(order_book_es=None, days=10):
    if order_book_es is None:
        order_book_es=random_order_book_()
    tasks = Tasks()
    #print(order_book_es)
    tasks.addOrderBooksList(order_book_es)
    tasks.registration_date = datetime.date.today() - datetime.timedelta(days=days)
    return tasks


def random_storage_of_book(mat=random_material_literature(), el=random_electronic_literature()):
    storage = []
    storage.extend(mat)
    storage.extend(el)
    return storage


def random_ticket(reader=None, tasks=None):
    if reader is None:
        reader = random_reader()
    if tasks is None:
        tasks = random_tasks()
    ticket = Ticket(date_of_registration=datetime.date.today() - datetime.timedelta(days=60), reader=reader)
    ticket.unique_key = reader.id
    ticket.tasks = [tasks]
    return ticket


def random_ticket_(reader=random_reader(), tasks=random_tasks()):
    ticket = Ticket(date_of_registration=datetime.date.today() - datetime.timedelta(days=60), reader=reader)
    ticket.unique_key = reader.id
    ticket.tasks = []
    for i in range(10):
        ticket.tasks.append(random_tasks())
    return ticket

